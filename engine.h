#ifndef ENGINE_H
#define ENGINE_H

#include "keys.h"

typedef struct {
  unsigned char r, g, b;
} color_t;

extern const color_t WHITE;
extern const color_t BLACK;
extern const color_t RED;
extern const color_t BLUE;
extern const color_t GREEN;
extern const color_t YELLOW;
extern const color_t MAGENTA;
extern const color_t CYAN;

typedef struct engine_s *engine_t;
typedef struct draw_context_s *draw_context_t;

typedef enum { LEFT, RIGHT } mouse_button_t;
typedef enum { BUTTON_PRESSED, BUTTON_RELEASED } mouse_action_t;
typedef enum { KEY_PRESSED, KEY_RELEASED } key_action_t;

typedef void (*draw_callback_t)(draw_context_t, void *);
typedef void (*update_callback_t)(void *, double);
typedef void (*mouse_moved_callback_t)(void *, int, int, int,
                                       int);  // x, y, dx, dy
typedef void (*mouse_wheel_callback_t)(void *, int, int);
typedef void (*mouse_button_callback_t)(void *, int, int, mouse_button_t,
                                        mouse_action_t);
typedef void (*window_resized_callback_t)(void *, int, int);
typedef void (*keyboard_callback_t)(void *, key_code_t, key_action_t, key_mod_t,
                                    int);  // key, action, mod, is-repeat

/**
 * @brief      Creates and initializes an engine instance
 *
 * @param      title   The window title
 * @param[in]  width   The window width
 * @param[in]  height  The window height
 *
 * @return     An opaque pointer to a new engine instance
 */
engine_t create_engine(char *title, int width, int height);

/**
 * @brief      Cleans up all resources used by the engine instance
 *
 * @param[in]  instance  The engine instance
 */
void destroy_engine(engine_t instance);

/**
 * @brief      Begins the main game loop, spinning off threads for drawing and
 *             updating as needed
 *
 * @param[in]  instance  The engine instance
 * @param      state     A state object that will be passed into update and draw
 *                       callbacks
 * @param[in]  draw      The draw callback
 * @param[in]  update    The update callback
 *
 * @return     A zero value if the engine's execution terminated due to an error
 *             condition, otherwise a non-zero value
 */
int run_engine(engine_t instance, void *state, draw_callback_t draw,
               update_callback_t update);

/**
 * @brief      Sets the mouse moved callback.
 *
 * @param[in]  instance  The instance
 * @param[in]  func      The function
 */
void set_mouse_moved_callback(engine_t instance, mouse_moved_callback_t func);

/**
 * @brief      Sets the mouse wheel callback.
 *
 * @param[in]  instance  The instance
 * @param[in]  func      The function
 */
void set_mouse_wheel_callback(engine_t instance, mouse_wheel_callback_t func);

/**
 * @brief      Sets the mouse button callback.
 *
 * @param[in]  instance  The instance
 * @param[in]  func      The function
 */
void set_mouse_button_callback(engine_t instance, mouse_button_callback_t func);

/**
 * @brief      Sets the window resized callback.
 *
 * @param[in]  instance  The instance
 * @param[in]  func      The function
 */
void set_window_resized_callback(engine_t instance,
                                 window_resized_callback_t func);

/**
 * @brief      Sets the keyboard callback.
 *
 * @param[in]  instance  The instance
 * @param[in]  func      The function
 */
void set_keyboard_callback(engine_t instance, keyboard_callback_t func);

/**
 * @brief      Sets the glyph scale.
 *
 * @param[in]  instance  The instance
 * @param[in]  scale     The scale
 */
void set_glyph_scale(engine_t instance, unsigned int scale);

/**
 * @brief      Sets the glyph padding.
 *
 * @param[in]  instance  The instance
 * @param[in]  x         Horizontal padding in pixels on either side of each
 *                       glyph
 * @param[in]  y         Padding in pixels above and below each glyph
 */
void set_glyph_padding(engine_t instance, unsigned int x, unsigned int y);

void get_cell_size(engine_t instance, int *width, int *height);

void draw_string(draw_context_t ctx, char *contents, int x, int y, color_t fg);

void draw_rect(draw_context_t ctx, int x, int y, int w, int h, color_t c);

void draw_line(draw_context_t ctx, int x1, int y1, int x2, int y2, color_t c);

void draw_circle(draw_context_t ctx, int x, int y, double radius, color_t c);

#define rect(x, y, w, h, c) draw_rect(ctx, x, y, w, h, c)

#define line(x1, y1, x2, y2, c) draw_line(ctx, x1, y1, x2, y2, c)

#define circle(x, y, r, c) draw_circle(ctx, x, y, r, c);

#define write(contents, x, y, c) draw_string(ctx, contents, x, y, c)

#endif