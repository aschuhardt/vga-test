#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "SDL.h"

#include <cp437.h>
#include "engine.h"

#define DEFAULT_SCALE 1
#define DEFAULT_PAD_X 1
#define DEFAULT_PAD_Y 4

#define RUN_SUCCESSFUL 1
#define RUN_FAILURE 0

#define UPDATE_QUIT 1

#define PRIMITIVE_BUFFER_SIZE 1024

#define PI 3.14159265358979323846

const color_t WHITE = {255, 255, 255};
const color_t BLACK = {0, 0, 0};
const color_t RED = {170, 0, 0};
const color_t BLUE = {0, 0, 170};
const color_t GREEN = {0, 170, 0};
const color_t YELLOW = {170, 170, 0};
const color_t MAGENTA = {170, 0, 170};
const color_t CYAN = {0, 170, 170};

#define LOG_SDL_ERROR() \
  printf("%s (%d): %s\n", __FILE__, __LINE__, SDL_GetError())

#define LOG_ERROR(e) fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, e)

typedef struct {
  unsigned int scale, pad_x, pad_y;
} glyph_settings_t;

typedef struct engine_s {
  glyph_settings_t glyph;
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Texture *glyph_texture;
  SDL_Thread *update_thread;
  SDL_mutex *quit_lock;
  SDL_mutex *state_lock;
  int update_quit;
  mouse_moved_callback_t mouse_moved_callback;
  mouse_button_callback_t mouse_button_callback;
  mouse_wheel_callback_t mouse_wheel_callback;
  window_resized_callback_t window_resized_callback;
  keyboard_callback_t keyboard_callback;
} engine_s;

typedef enum { PRIMITIVE_LINE, PRIMITIVE_RECT } primitive_type_t;

typedef struct {
  SDL_Rect dimensions;
  primitive_type_t type;
  color_t color;
} primitive_t;

typedef struct draw_context_s {
  engine_t engine;
  unsigned short primitive_count;
  primitive_t primitives[PRIMITIVE_BUFFER_SIZE];
} draw_context_s;

typedef struct {
  void *state;
  engine_t engine;
  update_callback_t update_callback;
} update_loop_container_t;

static SDL_Texture *create_glyph_texture(SDL_Renderer *const renderer) {
  unsigned long image_size;
  int w, h, channels;

  unsigned char *vga = cp437_get_texture(&image_size, &w, &h, &channels);
  if (!vga) {
    return NULL;
  }

  SDL_Surface *surf = SDL_CreateRGBSurfaceWithFormatFrom(
      vga, w, h, 24, channels * w, SDL_PIXELFORMAT_RGB24);

  SDL_Texture *texture = NULL;
  if (surf) {
    texture = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);
  }

  free(vga);
  return texture;
}

engine_t create_engine(char *const title, const int width, const int height) {
  engine_t instance = malloc(sizeof(engine_s));
  if (!instance) return NULL;

  instance->glyph.scale = DEFAULT_SCALE;
  instance->glyph.pad_x = DEFAULT_PAD_X;
  instance->glyph.pad_y = DEFAULT_PAD_Y;

  instance->keyboard_callback = NULL;
  instance->mouse_moved_callback = NULL;
  instance->mouse_button_callback = NULL;
  instance->mouse_wheel_callback = NULL;
  instance->window_resized_callback = NULL;

  instance->quit_lock = SDL_CreateMutex();
  instance->state_lock = SDL_CreateMutex();

  instance->update_quit = 0;

  if (SDL_Init(SDL_INIT_EVERYTHING)) {
    LOG_SDL_ERROR();
    return NULL;
  }

  instance->window =
      SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  if (!instance->window) {
    LOG_SDL_ERROR();
    destroy_engine(instance);
    return NULL;
  }

  instance->renderer =
      SDL_CreateRenderer(instance->window, -1,
                         SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (!instance->renderer) {
    LOG_SDL_ERROR();
    destroy_engine(instance);
    return NULL;
  }

  instance->glyph_texture = create_glyph_texture(instance->renderer);
  if (!instance->glyph_texture) {
    LOG_SDL_ERROR();
    destroy_engine(instance);
    return NULL;
  }

  return instance;
}

void destroy_engine(engine_t instance) {
  if (!instance) return;

  SDL_DestroyMutex(instance->quit_lock);
  SDL_DestroyMutex(instance->state_lock);

  if (instance->glyph_texture) SDL_DestroyTexture(instance->glyph_texture);
  if (instance->renderer) SDL_DestroyRenderer(instance->renderer);
  if (instance->window) SDL_DestroyWindow(instance->window);
  SDL_Quit();

  free(instance);
}

static int update_loop(void *container) {
  update_loop_container_t *cnt = ((update_loop_container_t *)container);
  engine_t engine = cnt->engine;
  void *state = cnt->state;
  update_callback_t update = cnt->update_callback;

  clock_t previous_time, current_time;
  previous_time = clock();

  for (;;) {
    current_time = clock();

    if (SDL_LockMutex(engine->quit_lock) == 0) {
      if (engine->update_quit) break;
      SDL_UnlockMutex(engine->quit_lock);
    } else {
      LOG_SDL_ERROR();
      LOG_ERROR("Couldn't lock quit-mutex from update loop");
      return RUN_FAILURE;
    }

    if (SDL_LockMutex(engine->state_lock) == 0) {
      update(state, (double)(current_time - previous_time) / CLOCKS_PER_SEC);
      SDL_UnlockMutex(engine->state_lock);
    } else {
      LOG_SDL_ERROR();
      LOG_ERROR("Couldn't lock state-mutex from update loop");
      return RUN_FAILURE;
    }

    SDL_Delay(20);
    previous_time = current_time;
  }

  return RUN_SUCCESSFUL;
}

int run_engine(engine_t instance, void *const state, draw_callback_t const draw,
               update_callback_t const update) {
  if (!instance) {
    return RUN_FAILURE;
  }

  update_loop_container_t loop_state = {state, instance, update};
  instance->update_thread =
      SDL_CreateThread(update_loop, "UpdateThread", &loop_state);

  draw_context_s draw_ctx;
  draw_ctx.engine = instance;

  SDL_Event event;
  for (;;) {
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          goto quit;
          break;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
          if (instance->keyboard_callback &&
              SDL_LockMutex(instance->state_lock) == 0) {
            SDL_Keysym key = event.key.keysym;
            instance->keyboard_callback(
                state, map_sdl_keycode(key.sym),
                event.type == SDL_KEYDOWN ? KEY_PRESSED : KEY_RELEASED,
                map_sdl_keymod(key.mod), event.key.repeat);
            SDL_UnlockMutex(instance->state_lock);
          }
          break;
        case SDL_MOUSEMOTION:
          if (instance->mouse_moved_callback &&
              SDL_LockMutex(instance->state_lock) == 0) {
            SDL_MouseMotionEvent motion = event.motion;
            instance->mouse_moved_callback(state, motion.x, motion.y,
                                           motion.xrel, motion.yrel);
            SDL_UnlockMutex(instance->state_lock);
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
          if (instance->mouse_button_callback &&
              SDL_LockMutex(instance->state_lock) == 0) {
            SDL_MouseButtonEvent mouse = event.button;
            instance->mouse_button_callback(
                state, mouse.x, mouse.y,
                mouse.button == SDL_BUTTON_RIGHT ? RIGHT : LEFT,
                mouse.state == SDL_PRESSED ? BUTTON_PRESSED : BUTTON_RELEASED);
            SDL_UnlockMutex(instance->state_lock);
          }
        case SDL_MOUSEWHEEL:
          if (instance->mouse_wheel_callback &&
              SDL_LockMutex(instance->state_lock) == 0) {
            SDL_MouseWheelEvent wheel = event.wheel;
            instance->mouse_wheel_callback(state, wheel.x, wheel.y);
            SDL_UnlockMutex(instance->state_lock);
          }
          break;
        case SDL_WINDOWEVENT:
          switch (event.window.event) {
            case SDL_WINDOWEVENT_SIZE_CHANGED:
              if (instance->window_resized_callback &&
                  SDL_LockMutex(instance->state_lock) == 0) {
                SDL_WindowEvent window = event.window;
                instance->window_resized_callback(state, window.data1,
                                                  window.data2);
                SDL_UnlockMutex(instance->state_lock);
              }
              break;
          }
        default:
          break;
      }
    }

    SDL_SetRenderDrawColor(instance->renderer, 0, 0, 0, 255);
    SDL_RenderClear(instance->renderer);

    if (SDL_LockMutex(instance->state_lock) == 0) {
      draw(&draw_ctx, state);
      SDL_UnlockMutex(instance->state_lock);
    } else {
      LOG_SDL_ERROR();
      LOG_ERROR("Couldn't lock state-mutex prior to draw call");
      break;
    }

    SDL_RenderPresent(instance->renderer);
  }

quit:
  if (SDL_LockMutex(instance->quit_lock) == 0) {
    instance->update_quit = UPDATE_QUIT;
    SDL_UnlockMutex(instance->quit_lock);
  } else {
    LOG_SDL_ERROR();
    LOG_ERROR("Couldn't lock quit-mutex from draw loop");
    return RUN_FAILURE;
  }

  int update_result = 0;
  SDL_WaitThread(instance->update_thread, &update_result);

  return RUN_SUCCESSFUL & update_result;
}

void set_glyph_scale(engine_t instance, unsigned int scale) {
  instance->glyph.scale = scale;
}

void get_cell_size(engine_t instance, int *width, int *height) {
  int x, y;
  cp437_get_glyph(' ', &x, &y, width, height);
  *width *= instance->glyph.scale;
  *width += instance->glyph.pad_x;
  *height *= instance->glyph.scale;
  *height += instance->glyph.pad_y;
}

void set_glyph_padding(engine_t instance, unsigned int x, unsigned int y) {
  instance->glyph.pad_x = x;
  instance->glyph.pad_y = y;
}

void draw_string(draw_context_t ctx, char *contents, int x, int y, color_t fg) {
  SDL_SetTextureColorMod(ctx->engine->glyph_texture, fg.r, fg.g, fg.b);

  unsigned long length = strlen(contents);
  unsigned int scale = ctx->engine->glyph.scale,
               pad_x = ctx->engine->glyph.pad_x;

  int gx, gy, gw, gh, offset = 0;
  for (int i = 0; i < length; ++i) {
    cp437_get_glyph((unsigned char)contents[i], &gx, &gy, &gw, &gh);

    const SDL_Rect src = {gx, gy, gw, gh},
                   dest = {x + offset, y, gw * (int)scale, gh * (int)scale};
    SDL_RenderCopy(ctx->engine->renderer, ctx->engine->glyph_texture, &src,
                   &dest);

    offset += gw * scale + pad_x;
  }
}

void draw_rect(draw_context_t ctx, int x, int y, int w, int h, color_t c) {
  SDL_SetRenderDrawColor(ctx->engine->renderer, c.r, c.g, c.b, 255);
  const SDL_Rect rect = {x, y, w, h};
  SDL_RenderFillRect(ctx->engine->renderer, &rect);
}

void draw_line(draw_context_t ctx, int x1, int y1, int x2, int y2, color_t c) {
  SDL_SetRenderDrawColor(ctx->engine->renderer, c.r, c.g, c.b, 255);
  SDL_RenderDrawLine(ctx->engine->renderer, x1, y1, x2, y2);
}

void draw_circle(draw_context_t ctx, int x, int y, double radius, color_t c) {
  SDL_SetRenderDrawColor(ctx->engine->renderer, c.r, c.g, c.b, 255);

  int circumference = (int)(2.0 * PI * radius) + 1;
  SDL_Point points[circumference];
  for (int i = 0; i < circumference; i++) {
    double theta = 2 * PI * ((double)i / circumference);
    SDL_Point point = {x + sin(theta) * radius, y + cos(theta) * radius};
    points[i] = point;
  }

  SDL_RenderDrawPoints(ctx->engine->renderer, points, circumference);
}

void set_mouse_moved_callback(engine_t instance, mouse_moved_callback_t func) {
  instance->mouse_moved_callback = func;
}

void set_mouse_wheel_callback(engine_t instance, mouse_wheel_callback_t func) {
  instance->mouse_wheel_callback = func;
}

void set_mouse_button_callback(engine_t instance,
                               mouse_button_callback_t func) {
  instance->mouse_button_callback = func;
}

void set_window_resized_callback(engine_t instance,
                                 window_resized_callback_t func) {
  instance->window_resized_callback = func;
}

void set_keyboard_callback(engine_t instance, keyboard_callback_t func) {
  instance->keyboard_callback = func;
}