#include <stdio.h>
#include <stdlib.h>

#include "engine.h"

#define UPS_BUFFER_SIZE 128

typedef struct {
  int cell_w, cell_h, win_w, win_h;
} state_obj;

void on_draw(draw_context_t ctx, void *state) {
  // state_obj *obj = (state_obj *)state;

  // for (int x = 0; x < obj->win_w; x += obj->cell_w) {
  //   line(x, 0, x, obj->win_h, CYAN);
  // }

  // for (int y = 0; y < obj->win_h; y += obj->cell_h) {
  //   line(0, y, obj->win_w, y, MAGENTA);
  // }

  for (int i = 10; i < 100; i++) {
    if (i % 3 == 0) {
      circle(200, 200, i, RED);
    }
  }
}

void on_mouse_move(void *state, int x, int y, int dx, int dy) {}

void on_update(void *state, double time) {}

void on_resize(void *state, int w, int h) {
  state_obj *obj = (state_obj *)state;
  obj->win_w = w;
  obj->win_h = h;
}

int main(int argc, const char **argv) {
  engine_t engine = create_engine("Testing", 800, 600);
  if (engine) {
    state_obj state = {0, 0, 800, 600};

    set_glyph_scale(engine, 3);
    get_cell_size(engine, &state.cell_w, &state.cell_h);

    set_mouse_moved_callback(engine, on_mouse_move);
    set_window_resized_callback(engine, on_resize);

    run_engine(engine, &state, on_draw, on_update);
    destroy_engine(engine);
  }
  return 0;
}